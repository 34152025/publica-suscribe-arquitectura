#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: monitor.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Perla Velasco, Yonathan Mtz.
# Version: 2.0.1 Mayo 2017
# Descripción:
#
#   Ésta clase define el rol del monitor, es decir, muestra datos, alertas y advertencias sobre los signos vitales de los adultos mayores.
#
#   Las características de ésta clase son las siguientes:
#
#                                            monitor.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |        Monitor        |  - Mostrar datos a los  |         Ninguna        |
#           |                       |    usuarios finales.    |                        |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |  print_notification()  |  - datetime: fecha en que|  - Imprime el mensa-  |
#           |                        |     se envió el mensaje. |    je recibido.       |
#           |                        |  - id: identificador del |                       |
#           |                        |     dispositivo que      |                       |
#           |                        |     envió el mensaje.    |                       |
#           |                        |  - value: valor extremo  |                       |
#           |                        |     que se desea notifi- |                       |
#           |                        |     car.                 |                       |
#           |                        |  - name_param: signo vi- |                       |
#           |                        |     tal que se desea no- |                       |
#           +------------------------+--------------------------+-----------------------+
#           |   format_datetime()    |  - datetime: fecha que se|  - Formatea la fecha  |
#           |                        |     formateará.          |    en que se recibió  |
#           |                        |                          |    el mensaje.        |
#           +------------------------+--------------------------+-----------------------+

#
#-------------------------------------------------------------------------


class Monitor:

    def print_notification(self, datetime, id, value, name_param, model):
        print("  ---------------------------------------------------")
        print("    ADVERTENCIA")
        print("  ---------------------------------------------------")
        print("    Se ha detectado un aumento en " + str(name_param) + " (" + str(value) + ")" + " a las " + str(self.format_datetime(datetime)) + " en el adulto mayor que utiliza el dispositivo " + str(model) + ":" + str(id))
        print("")
        print("")



    def print_notification_acc(self, datetime, id, value1, value2, value3, name_param, model):
        print("  ---------------------------------------------------")
        print("    ADVERTENCIA AlUIEN SE CALLÓ")
        print("  ---------------------------------------------------")
        print("    Se ha detectado una cambio en " + str(name_param) + " (" + str(value1) +","+str(value2)+str(value3)+")" + " a las " + str(self.format_datetime(datetime)) + " en el adulto mayor que utiliza el dispositivo " + str(model) + ":" + str(id))
        print("")
        print("")




    #Metodo que muestra el mensaje que manda el procesador de medicinas.
    #Atributos.
    #datetime: Muestra la fecha y hora que fue enviada la alerta.
    #id: Muestra el id del dipositivo que mando la alerta.
    #name_param: es el nombre del parametro datetime.
    #model: es el modelo del dispositivo que utiliza el usuario.
    #medicinasDosis: Es un string que contiene el medicamento y la dosis de los usuarios, es un string que fue concatenado previamente.
    def print_notification_med(self, datetime, id, name_param, model, medicinasDosis):
        print("  ---------------------------------------------------")
        print("    YA LE TOCAN LAS MEDICINAS!")
        print("  ---------------------------------------------------")
        
        print("    Ha llegado la hora de " + str(name_param) + " Las cuales son "+ medicinasDosis 
        + " a las " + str(self.format_datetime(datetime)) + " en el adulto mayor que utiliza el dispositivo " + str(model) + ":" + str(id))
        
        print("")
        print("")

    def format_datetime(self, datetime):
        values_datetime = datetime.split(':')
        f_datetime = values_datetime[3] + ":" + values_datetime[4] + " del " + \
            values_datetime[0] + "/" + \
            values_datetime[1] + "/" + values_datetime[2]
        return f_datetime
