#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: procesador_de_acelerometro.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Francisco Muro, Cesar Herrera, Joaquin Chavez, Carlos Vela, Carlos Quiñonez
# Version: 2.0.2 Marzo 2020
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                     procesador_de_acelerometro.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesa el tiempo
#                                        de la toma de        |    por el wearable     |
#           |     Procesador de     |    medicinas de los     |    Xiaomi My Band.     |
#           |        Medcinas       |    Adultos.             |  - Define la hora que- |
#           |                       |                         |     se deben de tomar  |
#           |                       |                         |    las medicinas.      |
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando la hora de   |
#           |                       |                         |    tomar una medicina  |
#           |                       |                         |    llego.              |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |                          |  - Recibe las horas   |
#           |       consume()        |          Ninguno         |    en las que las     |
#           |                        |                          |    medicinas deben de |
#           |                        |                          |    ser consumidas.    |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Detecta la hora en |
#           |                        |  - method: propio de     |    la que una medicina|
#           |                        |     Rabbit.              |    debera ser         |
#           |       callback()       |  - properties: propio de |    consumida.         |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#           |    string_to_json()    |  - string: texto a con-  |  - Convierte un string|
#           |                        |     vertir en JSON.      |    en un objeto JSON. |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
#-------------------------------------------------------------------------
import pika
import sys
sys.path.append('../')
from monitor import Monitor
import time


class ProcesadorDeMedicinas:

    def consume(self):
        try:
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
            # Se solicita un canal por el cuál se enviarán los signos vitales
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue='horaDeMedicinas', durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(on_message_callback=self.callback,queue='horaDeMedicinas')


            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close()  # Se cierra la conexiónaccelerometer
            sys.exit("Conexión finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")

    def callback(self, ch, method, properties, body):
        print("MONITOREANDO . . .")
        json_message = self.string_to_json(body)

        #Aqui extraemos la hora que nos envia la banda para poderla comparar con la hora que tenemos programada.
        hora = str(body)[21] + str(body)[22] + str(body)[23] + str(body)[24] + str(body)[25]

        #Esta linea solo muestra la hora concurrente de lo que corre el programa.
        #Ejemplo
        #Monitoreando...
        #19:30
        print(hora)

        #Esta linea saca la cantidad de medisinas que se le asgnaron al usuario.
        numMed = int(json_message['NumMed'])
        medicamentos = ""
        #Este for forma una cadena para poder concatenar el medicamento con sus dosis.
        for posicion in range(0, numMed):
            medicamentos = medicamentos +" medicamento = "+ str(json_message['medicina '+str(posicion+1)]) + " dosis = " + str(json_message['dosisMed '+str(posicion+1)]) + "  "

        #Estas lineas son hardcodeadas para las horas en la que tiene que tomar el medicamento cada adulto.
        if (hora=="07:30" or hora == "15:30" or hora == "10:30"):
            monitor = Monitor()
            monitor.print_notification_med(json_message['datetime'], json_message['id'], 'tomar las medicinas',
            json_message['model'], medicamentos)
            #Medicamentos = es la variable que nos ayuda a concatenar la informacino de medicamentos y dosis.
        
        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def string_to_json(self, string):
        message = {}
        string = string.decode('utf-8')
        string = string.replace('{', '')
        string = string.replace('}', '')
        values = string.split(', ')
        for x in values:
            v = x.split(': ')
            message[v[0].replace('\'', '')] = v[1].replace('\'', '')
        return message

if __name__ == '__main__':
    p_Medicinas= ProcesadorDeMedicinas()
    p_Medicinas.consume()
